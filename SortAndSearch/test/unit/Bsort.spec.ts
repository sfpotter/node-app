import { BSort }  from "../../src/BSort";

describe('Bsort Unit Test Case', () => {
    it('should return proper sort', () => {
        expect(BSort.sort([64, 34, 25, 12, 22, 11, 90])).toStrictEqual([11, 12, 22, 25, 34, 64, 90]);
    });

    it('should throw error if data incorrect', () => {
        expect(() => {
            BSort.sort([])
        }).toThrowError(new Error('Invalid input data for sort'))
    })
});