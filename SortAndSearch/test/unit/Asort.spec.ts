import { ASort }  from "../../src/ASort";

describe('Asort Unit Test Case', () => {
    it('should return proper sort', () => {
        expect(ASort.sort([9, 4, 8, 5, 2, 6, 3, 1, 7])).toStrictEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });

    it('should throw error if data incorrect', () => {
        expect(() => {
            ASort.sort([])
        }).toThrowError(new Error('Invalid input data for sort'))
    })
});