import {BSearch} from "../../src/BSearch";

describe('BSearch Unit Test Case', () => {
    it('should find proper index', () => {
        let searchClass = new BSearch().setData([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        expect(searchClass.indexOf(4)).toBe(3);
        expect(searchClass.indexOf(10)).toBe(9);
    });

    it('should return -1 if value not present', () => {
       let searchClass = new BSearch().setData([1, 3, 5, 8]);
       expect(searchClass.indexOf(9)).toBe(-1);
    });

    it('it should be proper calculate operations counter', () => {
        let searchClass = new BSearch().setData([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        searchClass.indexOf(4);
        expect(searchClass.operationsCounter).toBe(3);
        searchClass.indexOf(11);
        expect(searchClass.operationsCounter).toBe(4);
    });

    it('should return -1 if element doesnt exists', () => {
        let searchClass = new BSearch().setData([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        expect(searchClass.indexOf(555)).toBe(-1);
    });

    it('expect error if array doesnt sorted', () => {
        expect(() => {
            new BSearch().setData([9, 7, 12, 13, 3]).indexOf(10);
        }).toThrowError(new Error('Expected ascending for example: (1, 3, 5, 7) values given 9,7,12,13,3'))
    });
});