export class BSort {
    //bubble sort
    static sort(data: number[]): number[] {
        if (!Array.isArray(data) || data.length === 0) throw new Error('Invalid input data for sort');
        let result = data.slice();
        for(let i = 0; i < result.length; i++) {
            for(let j = 0; j < result.length - 1; j++) {
                if(result[j] > result[j + 1]) {
                    let swap = result[j];
                    result[j] = result[j + 1];
                    result[j + 1] = swap;
                }
            }
        }
        return result;
    }
}