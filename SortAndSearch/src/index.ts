import { ASort } from './ASort';
import {BSearch} from './BSearch';
import * as util from 'util'
const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
let binarySearch = new BSearch().setData(ASort.sort(unsorted));
const elementsToFind = [1, 5, 13, 27, 77];

elementsToFind.forEach((value) => {
    let index = binarySearch.indexOf(value);
   if (index === -1) {
       console.log(util.format('%s - NOT FOUND --> operations made: %s', value, binarySearch.operationsCounter));
   } else {
       console.log(util.format('%s FOUND at index %s --> operations made: %s', value, index, binarySearch.operationsCounter));
   }
});