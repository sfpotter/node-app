import * as util from 'util';

export class BSearch<T> {
    private data!: T[];
    private _operationsCounter = 0;
    private static instance: any;

    constructor() {
        if (!!BSearch.instance) {
            return BSearch.instance;
        }
        BSearch.instance = this;
        return this;
    }

    public setData(array: T[]) {
        this.data = array;
        return this;
    }

    public indexOf(val: T): ReturnType<BSearch<T>['search']> {
        return this.search(val)
    }

    public get operationsCounter():number {
        return this._operationsCounter;
    }

    private isSorted(array: T[]): boolean {
        for (let i = 1; i < array.length; i++) {
            if (array[i] < array[i - 1]) {
                return false
            }
        }
        return true
    }

    private validateData() {
        if (!this.isSorted(this.data)) {
            throw new Error(util.format('Expected ascending for example: (1, 3, 5, 7) values given %s', this.data));
        }
    }

    private search(searchValue: T): number {
        this.validateData();
        this._operationsCounter = 0;
        const arr = this.data;
        let start = 0;
        let end = arr.length;
        while (start <= end) {
            const mid = Math.floor((start + end) / 2);
            this._operationsCounter++;

            if (arr[mid] === searchValue) {
                return mid
            }
            if (arr[mid] > searchValue) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return -1
    }
}